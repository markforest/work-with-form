<?php
    include 'functions.php';
    $name = $email = '';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $name = valid_input($_POST['name']);
        $email = valid_input($_POST['email']);
    } else {
        $name = valid_input($_GET['name']);
        $email = valid_input($_GET['email']);
    }
    if(empty($name) || empty($email)){
        die("Error!!! This input not fill");
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Результат</title>
</head>
<body>
    <a href="index.php">Назад</a>
    <h1>Welcome <?= $name?></h1>
    <h2>Welcome <?= $email?></h2>
</body>
</html>