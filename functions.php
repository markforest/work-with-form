<?php
function valid_input($data)
{
    // '      <h1>Hello O\'Nill</h1>                             '
     $data = stripcslashes($data);
    // '      <h1>Hello O'Nill</h1>                             '
     $data = htmlspecialchars($data);
    // '      Hello O'Nill                             '
    $data = trim($data);
    // 'Hello O'Nill'
     return $data;
}
